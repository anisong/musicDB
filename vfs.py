"""A simple utility for constructing filesystem-like trees from beets
libraries.
"""
from __future__ import division, absolute_import, print_function
from collections import namedtuple
Node = namedtuple('Node', ['files', 'dirs'])
def _insert(node, path, itemid):
    """Insert an item into a virtual filesystem node."""
    if len(path) == 1:
        # Last component. Insert file.
        node.files[path[0]] = itemid
    else:
        # In a directory.
        dirname = path[0]
        rest = path[1:]
        if dirname not in node.dirs:
            node.dirs[dirname] = Node({}, {})
        _insert(node.dirs[dirname], rest, itemid)
def libtree(lib):
    """Generates a filesystem-like directory tree for the files
    contained in `lib`. Filesystem nodes are (files, dirs) named
    tuples in which both components are dictionaries. The first
    maps filenames to Item ids. The second maps directory names to
    child node tuples.
    """
    root = Node({}, {})
    for item in lib.items():
        dest = item.destination(fragment=True)
        parts = util.components(dest)
        _insert(root, parts, item.id)
    return root
